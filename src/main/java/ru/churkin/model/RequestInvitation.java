package ru.churkin.model;

import lombok.Data;

@Data
public class RequestInvitation {

    private String[] phoneNumbers;
    private String message;

}
