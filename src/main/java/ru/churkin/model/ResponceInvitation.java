package ru.churkin.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponceInvitation {
    boolean success;
    String message;

    public ResponceInvitation(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

}
