package ru.churkin.exception.message;

public class OverageLengthMessageException extends RuntimeException {
    public OverageLengthMessageException(String message) {
        super(message);
    }
}
