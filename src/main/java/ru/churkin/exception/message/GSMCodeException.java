package ru.churkin.exception.message;

public class GSMCodeException extends RuntimeException {
    public GSMCodeException(String message) {
        super(message);
    }
}
