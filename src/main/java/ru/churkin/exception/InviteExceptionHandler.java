package ru.churkin.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.churkin.enums.ErrorCode;
import ru.churkin.enums.ErrorMessage;
import ru.churkin.exception.message.EmptyMessageException;
import ru.churkin.exception.message.GSMCodeException;
import ru.churkin.exception.message.OverageLengthMessageException;
import ru.churkin.exception.phoneNumbers.*;

@ControllerAdvice
public class InviteExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InvalidPhoneNumberException.class)
    protected ResponseEntity<InviteException> handleInvalidPhoneNumberException() {
        return new ResponseEntity<>(createExceptionParameters(ErrorCode.BAD_REQUEST, ErrorMessage.PHONE_NUMBERS_INVALID_400),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(EmptyPhoneNumberException.class)
    protected ResponseEntity<InviteException> handleEmptyPhoneNumberException() {
        return new ResponseEntity<>(createExceptionParameters(ErrorCode.BAD_REQUEST, ErrorMessage.PHONE_NUMBERS_EMPTY_401),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(OveragePhoneNumbersException.class)
    protected ResponseEntity<InviteException> handleOveragePhoneNumbersException() {
        return new ResponseEntity<>(createExceptionParameters(ErrorCode.BAD_REQUEST, ErrorMessage.PHONE_NUMBERS_INVALID_402),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(OverageCountInvitationsPerDayException.class)
    protected ResponseEntity<InviteException> handleOverageCountInvitationsPerDayException() {
        return new ResponseEntity<>(createExceptionParameters(ErrorCode.BAD_REQUEST, ErrorMessage.PHONE_NUMBERS_INVALID_403),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(DuplicatePhoneNumbersException.class)
    protected ResponseEntity<InviteException> handleDuplicatePhoneNumbersException() {
        return new ResponseEntity<>(createExceptionParameters(ErrorCode.BAD_REQUEST, ErrorMessage.PHONE_NUMBERS_INVALID_404),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(EmptyMessageException.class)
    protected ResponseEntity<InviteException> handleEmptyMessageException() {
        return new ResponseEntity<>(createExceptionParameters(ErrorCode.BAD_REQUEST, ErrorMessage.MESSAGE_EMPTY_405),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(GSMCodeException.class)
    protected ResponseEntity<InviteException> handleGSMCodeException() {
        return new ResponseEntity<>(createExceptionParameters(ErrorCode.BAD_REQUEST, ErrorMessage.MESSAGE_INVALID_406),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(OverageLengthMessageException.class)
    protected ResponseEntity<InviteException> handleOverageLengthMessageException() {
        return new ResponseEntity<>(createExceptionParameters(ErrorCode.BAD_REQUEST, ErrorMessage.MESSAGE_INVALID_407),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(CommonInvitationException.class)
    protected ResponseEntity<InviteException> handleCommonInvitationException(CommonInvitationException e) {
        InviteException exception = new InviteException(500, e.getClazzz().toString(), e.getMessage());
        return new ResponseEntity<>(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(InternalSMSServiceException.class)
    protected ResponseEntity<InviteException> handleCommonInvitationException(InternalSMSServiceException e) {
        InviteException exception = new InviteException(500, ErrorCode.INTERNAL.name(), e.getMessage());
        return new ResponseEntity<>(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private InviteException createExceptionParameters(ErrorCode errorCode, ErrorMessage errorMessage){
        return new InviteException(errorMessage.getStatusCode(), errorCode.name(), errorMessage.getResultMessage());
    }

    public InviteException createExceptionParameters(String error, String message) {
        return new InviteException(500, error, message);
    }

    @Data
    @AllArgsConstructor
    private static class InviteException {
        private int status;
        private String error;
        private String message;
    }

}
