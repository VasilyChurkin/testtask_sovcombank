package ru.churkin.exception;

public class InternalSMSServiceException extends RuntimeException{
    private String className;

    public InternalSMSServiceException(String message, String className) {
        super(message);
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
