package ru.churkin.exception.phoneNumbers;

public class OverageCountInvitationsPerDayException extends RuntimeException {
    public OverageCountInvitationsPerDayException(String message) {
        super(message);
    }
}
