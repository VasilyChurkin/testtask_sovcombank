package ru.churkin.exception.phoneNumbers;

public class DuplicatePhoneNumbersException extends RuntimeException {
    public DuplicatePhoneNumbersException(String message) {
        super(message);
    }
}
