package ru.churkin.exception.phoneNumbers;

public class OveragePhoneNumbersException extends RuntimeException {
    public OveragePhoneNumbersException(String message) {
        super(message);
    }
}
