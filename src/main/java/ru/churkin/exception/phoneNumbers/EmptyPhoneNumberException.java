package ru.churkin.exception.phoneNumbers;

public class EmptyPhoneNumberException extends RuntimeException {
    public EmptyPhoneNumberException(String message) {
        super(message);
    }
}
