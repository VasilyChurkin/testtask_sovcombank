package ru.churkin.exception;

public class CommonInvitationException extends RuntimeException {
    private String className;

    public CommonInvitationException(String message, String className) {
        super(message);
        this.className = className;
    }

    public String getClazzz() {
        return className;
    }

    public void setClazzz(String className) {
        this.className = className;
    }

    public CommonInvitationException(String message) {
        super(message);
    }
}
