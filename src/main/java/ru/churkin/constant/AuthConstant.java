package ru.churkin.constant;

public class AuthConstant {

    /**
     * id приложения
     */
    public static final Integer API_ID = 4;
    /**
     * id пользователя, автора записи
     */
    public static final Integer USER_ID = 7;

    /**
     * Максимальное суммарное количество приглашений в сутки
     */
    public static final int MAX_COUNT_INVITATION = 128;
}
