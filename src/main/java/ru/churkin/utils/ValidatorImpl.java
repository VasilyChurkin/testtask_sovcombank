package ru.churkin.utils;

import org.springframework.stereotype.Component;
import ru.churkin.constant.AuthConstant;
import ru.churkin.exception.CommonInvitationException;
import ru.churkin.exception.message.EmptyMessageException;
import ru.churkin.exception.message.GSMCodeException;
import ru.churkin.exception.message.OverageLengthMessageException;
import ru.churkin.exception.phoneNumbers.*;

import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ValidatorImpl implements Validator {


    public String validatePhoneNumberPattern(String value) {
        final Pattern pattern = Pattern.compile("^((7)+([0-9]){10})$");
        Matcher matcher = pattern.matcher(value);
        if (!matcher.matches()) {
            throw new InvalidPhoneNumberException("PHONE_NUMBERS_INVALID: One or several phone numbers do not match with international format.");
        }
        return value;
    }

    public String[] validatePhoneNumbersPattern(String[] value) {
        for (int i = 0; i < value.length; i++) {
            validatePhoneNumberPattern(value[i]);
        }
        return value;
    }

    public String[] validateEmptyPhoneNumbers(String[] value) {
        if (value == null || value.length == 0) {
            throw new EmptyPhoneNumberException("PHONE_NUMBERS_EMPTY: Phone_numbers is missing.");
        }
        return value;
    }

    public String[] validateOveragePhoneNumbers(String[] value) {
        if (value.length > 16) {
            throw new OveragePhoneNumbersException("PHONE_NUMBERS_INVALID: Too much phone numbers, should be less or equal to 16 per request.");
        }
        return value;
    }

    public String[] validateDuplicatePhoneNumbers(String[] value) {
        String[] distinctList = Arrays.stream(value).distinct().toArray(String[]::new);
        if (!(value.length == distinctList.length)) {
            throw new DuplicatePhoneNumbersException("PHONE_NUMBERS_INVALID: Duplicate numbers detected.");
        }
        return value;
    }

    public String validateEmptyMessage(String message) {
        if (message == null || message.isEmpty() || message.trim().isEmpty()) {
            throw new EmptyMessageException("MESSAGE_EMPTY: Invite message is missing.");
        }
        return message;
    }

    public String validateMessageGSMCode(String message) {
        String regexp = "^([АаЕеКкОоТA-Za-z0-9 \\r\\n@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!\"#$%&'()*+,\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}\\\\\\[~\\]|\u20AC])*$";
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(message);
        if (!matcher.matches()) {
            throw new GSMCodeException("MESSAGE_INVALID: Invite message should contain only characters in 7-bit GSM encoding or Cyrillic letters as well.");
        }
        return message;
    }

    public String validateMessageLength(String message) {
        String gsmSymbols = "^([АаЕеКкОоТA-Za-z0-9 \\r\\n@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!\"#$%&'()*+,\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}\\\\\\[~\\]|\u20AC]){0,128}$";
        String gsmSymbolsLatin = "^([A-Za-z0-9 \\r\\n@£$¥!\"#$%&'()*+,\\-./:;<=>?^{}\\\\\\[~\\]|\u20AC]){0,160}$";
        Pattern pattern = Pattern.compile(gsmSymbols);
        Pattern patternLatin = Pattern.compile(gsmSymbolsLatin);
        Matcher matcher = pattern.matcher(message);
        Matcher matcherLatin = patternLatin.matcher(message);
        if (!matcher.matches() && !matcherLatin.matches()) {
            throw new OverageLengthMessageException("MESSAGE_INVALID: Invite message too long, should be less or equal to 128 characters of 7-bit GSM charset.");
        }
        return message;
    }

    @Override
    public String[] validatePhoneNumbers(String[] value) {
        // проверка на пустоту
        validateEmptyPhoneNumbers(value);
        // проверка на соответствие тефефонных номеров требуемому формату
        validatePhoneNumbersPattern(value);
        // проверка на дублирование номеров
        validateDuplicatePhoneNumbers(value);
        // проверка на превышение размера - 16 номеров
        validateOveragePhoneNumbers(value);
        return value;
    }

    @Override
    public String validateMessage(String message) {
        // проверка на пустоту
        validateEmptyMessage(message);
        // проверка на соответствие кодировке GSM 7-bit (03.38)
        validateMessageGSMCode(message);
        // проверка на превышение длины
        validateMessageLength(message);
        return message;
    }

    @Override
    public boolean validateCountInvitations(Integer countInvitations) {
        if (countInvitations == null) {
            throw new CommonInvitationException("Require non-null: countInvitations", this.getClass().getSimpleName());
        }
        if (countInvitations > AuthConstant.MAX_COUNT_INVITATION) {
            throw new OverageCountInvitationsPerDayException("PHONE_NUMBERS_INVALID_400: Too much phone numbers, should be less or equal to 128 per day.");
        }
        return true;
    }


}
