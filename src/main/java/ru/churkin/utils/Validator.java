package ru.churkin.utils;

public interface Validator {

    /**
     * Валидация списка номеров
     *
     * @param value список мобильных номеров незарегистрированных пользователей
     * @return отвалидированный список номеров
     */
    String[] validatePhoneNumbers(String[] value);

    /**
     * Валидация исходящего сообщения
     * @param message текст сообщения
     * @return отвалидированный текст сообщения
     */
    String validateMessage(String message);

    /**
     * Валидация количества приглашений за текущую дату
     * @param countInvitations количество приглашений за текущую дату
     * @return
     */
    boolean validateCountInvitations(Integer countInvitations);
}
