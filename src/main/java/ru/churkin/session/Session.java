package ru.churkin.session;

/**
 * Заглушка для получения ID пользователя
 */
public interface Session {

    int getUserId();

}
