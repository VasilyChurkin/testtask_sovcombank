package ru.churkin.session;

import org.springframework.stereotype.Component;
import ru.churkin.constant.AuthConstant;

/**
 * Заглушка для получения ID пользователя
 */
@Component
public class SessionImpl implements Session{
    @Override
    public int getUserId() {
        return AuthConstant.USER_ID;
    }
}
