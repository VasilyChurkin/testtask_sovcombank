package ru.churkin.enums;

public enum ErrorMessage {

    PHONE_NUMBERS_INVALID_400(400, "PHONE_NUMBERS_INVALID", "One or several phone numbers do not match with international format."),
    PHONE_NUMBERS_EMPTY_401(401, "PHONE_NUMBERS_EMPTY", "Phone_numbers is missing."),
    PHONE_NUMBERS_INVALID_402(402, "PHONE_NUMBERS_INVALID", "Too much phone numbers, should be less or equal to 16 per request"),
    PHONE_NUMBERS_INVALID_403(403, "PHONE_NUMBERS_INVALID", "Too much phone numbers, should be less or equal to 128 per day."),
    PHONE_NUMBERS_INVALID_404(404, "PHONE_NUMBERS_INVALID", "Duplicate numbers detected."),
    MESSAGE_EMPTY_405(405, "MESSAGE_EMPTY", "Invite message is missing."),
    MESSAGE_INVALID_406(406, "MESSAGE_INVALID", "Invite message should contain only characters in 7-bit GSM encoding or Cyrillic letters as well."),
    MESSAGE_INVALID_407(407, "MESSAGE_INVALID", "Invite message too long, should be less or equal to 128 characters of 7-bit GSM charset."),
    SMS_SERVICE(500, "SMS_SERVICE", null);

    private String errorMessage;
    private int statusCode;
    private String detailCode;
    private String resultMessage;

    ErrorMessage(int statusCode, String detailCode, String errorMessage) {
        this.statusCode = statusCode;
        this.detailCode = detailCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResultMessage() {
        return detailCode +": "+ errorMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }
}
