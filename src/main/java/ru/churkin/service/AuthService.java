package ru.churkin.service;

public interface AuthService {

    /**
     * Сервис для отправки приглашения по СМС
     * @param phoneNumbers Список телефонных номеров
     * @param message сообщение
     */
    void sendInvites(String[] phoneNumbers, String message);

}
