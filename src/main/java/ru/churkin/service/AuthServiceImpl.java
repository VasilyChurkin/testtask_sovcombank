package ru.churkin.service;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.churkin.constant.AuthConstant;
import ru.churkin.repository.AuthRepository;
import ru.churkin.utils.Validator;

@Service
@NoArgsConstructor
public class AuthServiceImpl implements AuthService {

    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private Validator validator;

    @Override
    public void sendInvites(String[] phoneNumbers, String message) {
        validator.validateMessage(message);
        validator.validatePhoneNumbers(phoneNumbers);

        Integer countInvitations = authRepository.getCountInvitations(AuthConstant.API_ID);
        if (validator.validateCountInvitations(countInvitations)) {
            authRepository.saveInvites(phoneNumbers);
        }
    }
}
