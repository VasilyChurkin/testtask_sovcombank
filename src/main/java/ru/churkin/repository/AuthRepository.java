package ru.churkin.repository;

public interface AuthRepository {

    /**
     * Добавляет приглашения в БД. <br/>
     *
     * @param phoneNumbers Cписок мобильных номеров незарегистрированных пользователей
     *                     </br>
     */
    void saveInvites(String[] phoneNumbers);

    /**
     * Получает количество уже отправленных приглашений в течение дня и в рамках определённого приложения.<br/>
     *
     * @param apiId id приложения (всегда равно 4).
     * @return количество уже отправленных приглашений в течение дня и в рамках определённого приложения.
     */
    Integer getCountInvitations(int apiId);

}
