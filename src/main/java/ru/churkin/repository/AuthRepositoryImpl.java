package ru.churkin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.churkin.enums.ErrorMessage;
import ru.churkin.exception.CommonInvitationException;
import ru.churkin.exception.InternalSMSServiceException;
import ru.churkin.session.Session;

import java.sql.*;

@Repository
public class AuthRepositoryImpl implements AuthRepository {

    @Autowired
    private Session session;

    private static final String URL = "jdbc:mysql://host:port/database";
    private static final String USER_NAME = "user";
    private static final String PASSWORD = "pass";

    /**
     * Добавляет приглашения в БД. <br/>
     * Использует встроенную процедуру <br/>
     * FUNCTION invite(user_id integer, phones text[]) - фиксирует факт отправки приглашений.<br/>
     * user_id - id пользователя, автора записи (всегда равен 7);<br/>
     * phones - массив номеров приглашаемых пользователей <br/>
     *
     * @param phoneNumbers Cписок мобильных номеров незарегистрированных пользователей
     *                     </br>
     */
    @Override
    public void saveInvites(String[] phoneNumbers) {
        java.sql.Array phoneNumbersArray;
        try (Connection connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
             CallableStatement statement = connection.prepareCall("{ call invite(?, ?) }")) {
            statement.setInt(1, session.getUserId());
            phoneNumbersArray = connection.createArrayOf("phone_numbers", phoneNumbers);
            statement.setArray(2, phoneNumbersArray);
            statement.execute();
        } catch (SQLException e) {
            throw new InternalSMSServiceException(ErrorMessage.SMS_SERVICE.name() + ": Error saving invites in database",
                    this.getClass().getSimpleName());
        }
    }

    /**
     * Получает количество уже отправленных приглашений в течение дня и в рамках определённого приложения.<br/>
     * Использует встроенную процедуру <br/>
     * FUNCTION getcountinvitations(apiid integer)<br/>
     *
     * @param apiId id приложения (всегда равно 4).
     * @return количество уже отправленных приглашений в течение дня и в рамках определённого приложения.
     */
    @Override
    public Integer getCountInvitations(int apiId) {
        Integer result = null;
        try (Connection connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
             CallableStatement statement = connection.prepareCall("{ call getcountinvitations(?) }")) {
            statement.setInt(1, apiId);
            statement.registerOutParameter(2, Types.INTEGER);
            statement.execute();
            result = statement.getInt(2);
        } catch (SQLException e) {
            throw new CommonInvitationException("Error get CountInvitations from database",
                    this.getClass().getSimpleName());
        }
        return result;
    }

}
