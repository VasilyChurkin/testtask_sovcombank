package ru.churkin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.churkin.model.RequestInvitation;
import ru.churkin.model.ResponceInvitation;
import ru.churkin.service.AuthService;

@RestController
@RequestMapping(value = "/")
public class AuthControllerImpl {

    @Autowired
    private AuthService authService;

    @RequestMapping(path="/sendInvites", method = RequestMethod.POST)
    @ResponseBody
    public  ResponceInvitation sendInvites(@RequestBody RequestInvitation invitation){
        authService.sendInvites(invitation.getPhoneNumbers(),invitation.getMessage());
        ResponceInvitation responceInvitation =
                new ResponceInvitation(true, "Users invite success with parameters: " + invitation);
        return responceInvitation;
    }

}
