package ru.churkin.utils;

import org.junit.jupiter.api.Test;
import ru.churkin.exception.message.EmptyMessageException;
import ru.churkin.exception.message.GSMCodeException;
import ru.churkin.exception.message.OverageLengthMessageException;
import ru.churkin.exception.phoneNumbers.DuplicatePhoneNumbersException;
import ru.churkin.exception.phoneNumbers.EmptyPhoneNumberException;
import ru.churkin.exception.phoneNumbers.InvalidPhoneNumberException;
import ru.churkin.exception.phoneNumbers.OveragePhoneNumbersException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidatorImplTest {

    ValidatorImpl validatorImpl = new ValidatorImpl();

    @Test
    public void testValidatePhoneNumber() {
        assertEquals("79032405656", validatorImpl.validatePhoneNumberPattern("79032405656"));
    }

    @Test
    public void testValidatePhoneNumberFail() {
        Exception exception1 = assertThrows(InvalidPhoneNumberException.class, () -> {
            validatorImpl.validatePhoneNumberPattern("89032404656");
        });
        Exception exception2 = assertThrows(InvalidPhoneNumberException.class, () -> {
            validatorImpl.validatePhoneNumberPattern("+79032404656");
        });
        Exception exception3 = assertThrows(InvalidPhoneNumberException.class, () -> {
            validatorImpl.validatePhoneNumberPattern("A79032404656");
        });
        String expectedMessage = "PHONE_NUMBERS_INVALID";
        assertTrue(exception1.getMessage().contains(expectedMessage));
        assertTrue(exception2.getMessage().contains(expectedMessage));
        assertTrue(exception3.getMessage().contains(expectedMessage));
    }

    @Test
    public void whenValidatePhoneNumbersThrown_thenAssertionSucceeds() {
        Exception exception1 = assertThrows(EmptyPhoneNumberException.class, () -> {
            validatorImpl.validateEmptyPhoneNumbers(new String[]{});
        });
        Exception exception2 = assertThrows(EmptyPhoneNumberException.class, () -> {
            validatorImpl.validateEmptyPhoneNumbers(null);
        });
        String expectedMessage = "PHONE_NUMBERS_EMPTY";
        assertTrue(exception1.getMessage().contains(expectedMessage));
        assertTrue(exception2.getMessage().contains(expectedMessage));
    }

    @Test
    public void testValidateOveragePhoneNumbers() {
        String[] value = new String[]{"9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567", "9031234567",
        };
        Exception exception = assertThrows(OveragePhoneNumbersException.class, () -> {
            validatorImpl.validateOveragePhoneNumbers(value);
        });
        String expectedMessage = "PHONE_NUMBERS_INVALID: Too much phone numbers, should be less or equal to 16 per request.";
        assertTrue(exception.getMessage().equals(expectedMessage));

    }

    @Test
    public void testValidateDuplicatePhoneNumbers() {
        String[] incomeArray = new String[]{"79151234567", "79167894561"};
        String[] resultArray = validatorImpl.validateDuplicatePhoneNumbers(incomeArray);
        assertTrue(incomeArray.length == resultArray.length);
        for (int i = 0; i < incomeArray.length; i++) {
            assertTrue(incomeArray[i].equals(resultArray[i]));
        }
    }

    @Test
    public void whenValidateDuplicatePhoneNumbersThrows_thenTestSucceeds() {
        String[] incomeArray = new String[]{"79151234567", "79167894561", "79167894561"};

        Exception exception = assertThrows(DuplicatePhoneNumbersException.class, () -> {
            validatorImpl.validateDuplicatePhoneNumbers(incomeArray);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "PHONE_NUMBERS_INVALID";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testValidateEmptyMessage(){
        String incomeMessage = "123";
        assertTrue(incomeMessage.equals(validatorImpl.validateEmptyMessage(incomeMessage)));
    }

    @Test
    public void whenValidateEmptyMessageThrows_thenTestSucceds(){
        Exception exception1 = assertThrows(EmptyMessageException.class, () ->{
            validatorImpl.validateEmptyMessage("");
        });
        Exception exception2 = assertThrows(EmptyMessageException.class, () ->{
            validatorImpl.validateEmptyMessage(null);
        });
        Exception exception3 = assertThrows(EmptyMessageException.class, () ->{
            validatorImpl.validateEmptyMessage("    ");
        });
        String expectedMessage = "MESSAGE_EMPTY";
        assertTrue(exception1.getMessage().contains(expectedMessage));
        assertTrue(exception2.getMessage().contains(expectedMessage));
        assertTrue(exception3.getMessage().contains(expectedMessage));
    }

    @Test
    public void testValidateMessageGSMCode(){
        assertEquals("qwe", validatorImpl.validateMessageGSMCode("qwe"));
        assertEquals("qw e ", validatorImpl.validateMessageGSMCode("qw e "));
        assertEquals("qw 568e", validatorImpl.validateMessageGSMCode("qw 568e"));
        assertEquals("Cyrillic A Latin A", validatorImpl.validateMessageGSMCode("Cyrillic A Latin A"));
        assertEquals("èéùìòÇØøÅå", validatorImpl.validateMessageGSMCode("èéùìòÇØøÅå"));
    }

    @Test
    public void whenValidateMessageGSMCodeTrows_thenTestSucceed(){
        Exception exception1 = assertThrows(GSMCodeException.class, () -> {
            validatorImpl.validateMessageGSMCode("йцук");
        });
        String expectedMessage = "MESSAGE_INVALID";
        assertTrue(exception1.getMessage().contains(expectedMessage));
    }

    @Test
    public void testValidateMessageLength(){
        assertEquals("qwer", validatorImpl.validateMessageLength("qwer"));
        assertEquals("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq",
                validatorImpl.validateMessageLength("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"));
        assertEquals("аОоOo123456frtgbhqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq",
                validatorImpl.validateMessageLength("аОоOo123456frtgbhqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"));
    }

    @Test
    public void whenValidateMessageLengthTrows_thenTestSucceed(){
        Exception exception1 = assertThrows(OverageLengthMessageException.class, () -> {
            validatorImpl.validateMessageLength("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
        });
        Exception exception2 = assertThrows(OverageLengthMessageException.class, () -> {
            validatorImpl.validateMessageLength("Ооzsssssasasssssqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
        });
        String expectedMessage = "MESSAGE_INVALID";
        assertTrue(exception1.getMessage().contains(expectedMessage));
        assertTrue(exception2.getMessage().contains(expectedMessage));
    }


}