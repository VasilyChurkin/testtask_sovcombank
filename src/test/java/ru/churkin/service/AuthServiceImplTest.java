package ru.churkin.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import ru.churkin.repository.AuthRepository;
import ru.churkin.service.config.TestConfiguration;
import ru.churkin.utils.Validator;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class, loader = AnnotationConfigContextLoader.class)
class AuthServiceImplTest {

    @Autowired
    AuthService authService;

    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private Validator validator;

    private final int apiId = 4;
    private final Integer countInvitations = 120;
    private final String[] phoneNumbers = {"79161234645", "78549632154"};
    private final String message = "abcdef";

    @Test
    public void testSendInvitesSuccess() {
        Mockito.when(authRepository.getCountInvitations(apiId))
                .thenReturn(countInvitations);
        Mockito.when(validator.validateCountInvitations(countInvitations)).thenReturn(true);
        authService.sendInvites(phoneNumbers, message);

        Mockito.verify(authRepository, times(1)).saveInvites(phoneNumbers);
    }

    @Test
    public void testSendInviteFail_whenOverageCountInvitation() {
        Mockito.when(authRepository.getCountInvitations(apiId))
                .thenReturn(1298);
        Mockito.when(validator.validateCountInvitations(authRepository.getCountInvitations(apiId))).thenReturn(false);
        authService.sendInvites(phoneNumbers, message);

        Mockito.verify(authRepository, never()).saveInvites(phoneNumbers);
    }

}