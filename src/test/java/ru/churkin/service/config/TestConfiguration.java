package ru.churkin.service.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.churkin.repository.AuthRepository;
import ru.churkin.service.AuthService;
import ru.churkin.service.AuthServiceImpl;
import ru.churkin.utils.Validator;

@Configuration
public class TestConfiguration {

    @Bean
    public AuthService authService() {
        return new AuthServiceImpl();
    }

    @Bean
    AuthRepository authRepository() {
        return Mockito.mock(AuthRepository.class);
    }

    @Bean
    Validator validator() {
        return Mockito.mock(Validator.class);
    }
}
